#pragma once
#include <stdio.h>
#include "sqlite3.h"
#include <iostream>
#include <vector>
#include <string>
#include "Helper.h"
#include <iomanip>
#include <sstream>
#include"Game.h"
#include"Room.h"
#include"User.h"
#include"DataBase.h"
#include <iostream>
#include <stdlib.h>
using std::string;
using std::vector;

int counter = 0;
bool passIsC = false;
string passG;
vector<string> vec;
vector<string> vec2;
string Name;

DataBase::DataBase()
{
	try
	{
		sqlite3* db;
		sqlite3_open("FirstPart.db", &db);
	}
	catch (string e)
	{
		cout << "Error!" << e << endl;
	}
}

DataBase::~DataBase()
{
	sqlite3_close(db);
}

bool DataBase::isUserExists(string userName)
{
	int rc = 0;
	char *zErrMsg = 0;
	string ss = "Select count(*) from t_users where username =  "+ userName;
	const char* css = ss.c_str();
	rc = sqlite3_exec(db, css, callbackCount, 0, &zErrMsg);
	if (rc)
		return true;
	else
		return false;
}

bool DataBase::addNewUser(string userName, string password, string email)
{
	string ss = ("Insert into t_users values(" + userName + password + email + ")");
	const char* css = ss.c_str();
	if (sqlite3_exec(db, css, NULL, 0, NULL))
		return false;
	return true;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	vector <Question*> questionV;
	string ss = "Select * question from t_question limit " + questionsNo;	
	const char* css = ss.c_str();
	sqlite3_exec(db, css, callbackQuestions, NULL, NULL);
	return questionV;
}

int DataBase::insertNewGame()
{
	time_t  timev;
	time(&timev);
	string ss = "Select * from t_games ";
	const char* css = ss.c_str();
	sqlite3_exec(db, css, callbackCount, NULL, NULL);
	counter++;
	string ss1 = "Insert into t_games values( " + to_string(counter) + "0" + to_string(timev) + "0" + " )";
	const char* css1 = ss1.c_str();
	sqlite3_exec(db, css1, NULL, NULL, NULL);

	return counter;
}

bool DataBase::updateGameStatus(int gameId)
{
	time_t  timev;
	time(&timev);
	string ss = ("Update t_games set status = 1 and end_time = " + to_string(timev) + "where game_id = " + to_string(gameId));
	const char* css = ss.c_str();
	if (sqlite3_exec(db, css, NULL, NULL, NULL))
		return false;
	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	string ss = ("Insert t_players_answers values( " + to_string(gameId) + username + to_string(questionId) + answer + to_string(isCorrect) + to_string(answerTime) + " )");
	const char* css = ss.c_str();
	if (sqlite3_exec(db, css, NULL, NULL, NULL))
		return false;
	return true;
}

int DataBase::callbackCount(void* notUsed, int argc, char** argv, char** azCol)
{
	counter = argc;
	return(0);
}

int DataBase::callbackQuestions(void* data, int argc, char** argv, char** azCol)
{
	counter = argc;
	return(0);
}
int DataBase::callbackPass(void* data, int argc, char** argv, char** azCol)
{
	if (argv[0] == passG)
		passIsC = true;
	return(0);
}

bool DataBase::isUserAndPassMatch(string userName, string password)
{
	passG = password;
	string ss = "Select password from t_users where username = " + userName;
	const char* css = ss.c_str();
	sqlite3_exec(db, css, callbackPass, NULL, NULL);
	return passIsC;
}

int DataBase::callbacklPersonalStatus(void* data, int argc, char** argv, char** azCol)
{
	vec.push_back(argv[0]);   
	vec.push_back(argv[1]);   
	return 0;
}

vector<string> DataBase::getBestScores()
{
	string ss = "Select * username as us from t_players_answers where min(answr_time) and is_correct = true";
	const char* css = ss.c_str();
	sqlite3_exec(db, css, callbackBestScores, NULL, NULL);

	return(getPersonalStatus(Name));
}

int DataBase::callbackBestScores(void* data, int argc, char** argv, char** azCol)
{
	Name = argv[0];
	return 0;
}

vector<string> DataBase::getPersonalStatus(string userName)
{
	string ss = "Select * password and email from t_users where username = " + userName;
	const char* css = ss.c_str();
	sqlite3_exec(db, css, callbacklPersonalStatus, NULL, NULL);

	return vec;
}