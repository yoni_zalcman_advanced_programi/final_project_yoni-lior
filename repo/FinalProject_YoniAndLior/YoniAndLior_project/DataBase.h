#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>
using std::string;
using std::vector;

class Question;
class sqlite3;

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);

private:
	int static callbackCount(void*, int, char**, char**);
	int static callbackQuestions(void*, int, char**, char**);
	int static callbackBestScores(void*, int, char**, char**);
	int static callbacklPersonalStatus(void*, int, char**, char**);
	int static callbackPass(void* data, int argc, char** argv, char** azCol);
	sqlite3* db;
};
