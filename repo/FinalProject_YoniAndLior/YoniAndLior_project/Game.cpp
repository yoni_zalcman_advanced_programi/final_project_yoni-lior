#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Helper.h"
#include <iomanip>
#include <sstream>
#include"Game.h"
#include"Room.h"
#include"User.h"
#include"DataBase.h"
#include "Question.h"
using std::string;
using std::vector;

class DataBase;

#define MAX_USERS 5
#define MAX_Q 5

int ID = 0;

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) :_db(db)
{
	_players = players;
	_db = db;
	_questions_no = questionsNo;
	DataBase();
	try
	{
		ID=db.insertNewGame();
		db.initQuestions(questionsNo);
	}
	catch(string e)
	{
		throw(e);
	}
	for (int i = 0; i < players.size(); i++)
	{
		_players[i]->setGame(this);
	}
}

Game::~Game()
{
	for (auto i = _players.begin(); i != _players.end(); i++)
	{
		_players.erase(i);
	}

	for (auto i = _questions.begin(); i != _questions.end(); i++)
	{
		_questions.erase(i);
	}
}

void Game::sendFirstQuestion()
{
	sendQuestonToAllUsers();
}

void Game::handleFinishGame()
{
	//_db->updateGameStatus();
	string meesage = "121";
	for (int i = 0; i < _players.size(); i++)
	{
		_players[i]->send(meesage);
		_players[i]->setGame(nullptr);
	}
}

bool Game::handleNextTurn()
{
	bool status;
	if (_players[0] != NULL)
	{
		if (_currQuestionIndex == MAX_USERS)
		{
			if (_questions_no == MAX_Q)
			{
				handleFinishGame();
			}
			_questions_no++;
			sendQuestonToAllUsers();
		}
		handleNextTurn();
		status = TRUE;
	}
	else
	{
		handleFinishGame();
		status = FALSE;
	}
	return(status);
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	string meesage = "120";
	bool curr = false;
	_questions_no++;
	if (_players[0] != NULL)
	{
		if (_currQuestionIndex == answerNo)
		{
			_results[user->getUsername()]++;
			user->send(meesage);
			curr = true;
		}
		_db.addAnswerToPlayer(ID, user->getUsername(), _questions[_questions_no]->getId(), *(_questions[_questions_no]->getAnswers() + answerNo), curr, time);
		return FALSE;
	}
	else
	{
		handleFinishGame();
		return TRUE;
	}
}

bool Game::leaveGame(User* currUser)
{
	bool status;
	for (auto i = _players.begin(); i != _players.end(); i++)
	{
		if (*i == currUser)
		{
			_players.erase(i);
			handleNextTurn();
			status = TRUE;
		}
	}
	handleNextTurn();
	status = FALSE;

	return(status);
}

int Game::getID()
{
	return(ID);
}

bool Game::insertGameToDB()
{
	return(true);
}

void Game::initQuestionsFromDB()
{

}

void Game::sendQuestonToAllUsers()
{
	string meesage = "118";
	_currentTurnAnswers = 0;
	for (int i = 0; i < _players.size(); i++)
	{
		_players[i]->send(meesage);
	}
}

