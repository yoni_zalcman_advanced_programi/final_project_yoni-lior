#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Helper.h"
#include"Game.h"
#include"Room.h"
#include"User.h"
#include"Question.h"
#include<time.h>
#include "Validator.h"
#include "DataBase.h"
using std::string;
using std::vector;

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	int count = 0, one=0,two=0,three=0,four=0;
	_question = question;
	_id = id;
	int ind;
	srand(time(NULL));
	while (1)
	{
		ind = rand() % 4;
		if (!one||!two||!three||!four)
		{
			if (!one)
			{
				_answers[count] = correctAnswer;
				one = 1;
				_correctAnsweIndex = count;
				count++;
			}
		}
		if (ind == 1)
		{
			if (!two)
			{
				_answers[count] = answer2;
				two = 1;
				count++;
			}
		}
		if (ind == 2)
		{
			if (!three)
			{
				_answers[count] = answer3;
				three = 1;
				count++;
			}
		}
		if (ind == 3)
		{
			if (!four)
			{
				_answers[count] = answer4;
				four = 1;
				count++;
			}
		}
	}
}

string Question::getQuestion()
{
	return(_question);
}

string* Question::getAnswers()
{
	return(_answers);
}

int Question::getCorrectAnswerIndex()
{
	return(_correctAnsweIndex);
}

int Question::getId()
{
	return(_id);
}