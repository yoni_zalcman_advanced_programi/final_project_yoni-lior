#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>

using std::string;
using std::vector;


class Question
{
public:
	Question(int, string, string, string, string, string);
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	string _answers[4];
	int _correctAnsweIndex;
	int _id;

};