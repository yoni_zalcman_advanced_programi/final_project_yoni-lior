#include<iostream>
#include<stdio.h>
#include "WSAInitializer.h"	
#include "TriviaServer.h"
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "User.h"
#include"Question.h"
#include"Game.h"
#include "Validator.h"
#include "DataBase.h"

RecievedMessage::RecievedMessage(SOCKET soc, int code)
{
	_sock = soc;
	_messageCode = code;
}
RecievedMessage::RecievedMessage(SOCKET soc, int code, vector<string> values)
{
	_sock = soc;
	_messageCode = code;
	_values = values;
}
RecievedMessage:: ~RecievedMessage()
{}
SOCKET RecievedMessage::getSock()
{
	return _sock;
}
User* RecievedMessage::getUser()
{
	return _user;
}
void RecievedMessage::setUser(User* user)
{
	_user = user;
}
int RecievedMessage::getMessageCode()
{
	return _messageCode;
}
vector<string>& RecievedMessage::getValues()
{
	return _values;
}