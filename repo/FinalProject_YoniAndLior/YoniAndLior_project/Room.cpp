#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>

#include "WSAInitializer.h"	
#include "TriviaServer.h"
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "User.h"
#include"Question.h"
#include"Game.h"
#include "Validator.h"
#include "DataBase.h"
using std::string;

#define MAX_USERS 5

Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionTime = questionsNo;
	_questionNo = questionTime;
	//������ ������ �����
}

Room ::~Room()
{}

string Room::getName()
{
	return _name;
}
int Room::getId()
{
	return _id;
}
vector <User*> Room::getUsers()
{
	return _users;
}
string Room::getUsersAsString(vector<User*> userList, User* excludeUser)
{
	string str(userList.begin(), userList.end());
	return(str);
}

string Room::getUsersListMessage()
{
	SOCKET sc;
	string users = getUsersAsString(_users, _admin);
	Helper::sendData(_admin->getSocket(), "108");
	return("108");
}

void Room::sendMessage(string message, User* excludeUser)
{
	for (auto i = _users.begin(); i != _users.end(); i++)
	{
		excludeUser->send(message);
	}
}

void Room::sendMessage(string message)
{
	sendMessage(message, NULL);
}

bool Room::joinRoom(User* user)
{
	bool join;
	string updateUsers;
	if (_maxUsers != MAX_USERS)
	{
		_users.push_back(user);
		join = TRUE;
		Helper::sendData(user->getSocket(), "110");
		sendMessage(getUsersListMessage());
	}
	else
	{
		join = FALSE;
		Helper::sendData(user->getSocket(), "Failed");
		sendMessage(getUsersListMessage());
	}
	return(join);
}

void Room::leaveRoom(User* user)
{
	for (int i = 0; i < _users.size(); i++)
	{
		if (_users[i] == user)
		{
			_users.erase(_users.begin() + i);
			//Helper::sendData(user->getSocket(), Protocol::LeaveRoomA);
			sendMessage(getUsersListMessage());
			break;
		}
	}
}

int Room::closeRoom(User* user)
{
	int ret;
	string userName1 = user->getUsername();
	string userName2 = _admin->getUsername();
	if (userName1.compare(userName2) == 0)
	{
		Helper::sendData(_admin->getSocket(), "116");
		ret = _id;
	}
	else
	{
		Helper::sendData(_admin->getSocket(), "Failed");
		ret = -1;
	}

	return(ret);
}