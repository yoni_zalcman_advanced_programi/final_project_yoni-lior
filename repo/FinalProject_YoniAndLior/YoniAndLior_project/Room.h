#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>
using std::string;
using std::vector;

class RecievedMessage;
class Helper;

class Room
{
public:
	Room(int, User*, string, int, int, int);
	~Room();
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	vector <User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();

private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string, User*);
	void sendMessage(string);
};
