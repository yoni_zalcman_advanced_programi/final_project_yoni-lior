#pragma comment (lib, "ws2_32.lib")

#include<iostream>
#include<stdio.h>
#include<exception>

#include "WSAInitializer.h"	
#include "TriviaServer.h"
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "User.h"
#include"Question.h"
#include"Game.h"
#include "sqlite3.h"
#include "Validator.h"
#include "DataBase.h"

using namespace std;

void main()
{
	try
	{
		WSAInitializer wsaInit;
		TriviaServer server;
		server.serve();
	}
	catch (exception& e)
	{
		cout << "Error occured: " << e.what() << endl;
	}
	system("PAUSE");
}