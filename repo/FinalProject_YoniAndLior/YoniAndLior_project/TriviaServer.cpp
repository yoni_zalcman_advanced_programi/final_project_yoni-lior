#include <stdio.h>
#include <iostream>
#include<exception>
#include "WSAInitializer.h"	
#include "TriviaServer.h"
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "User.h"
#include "Question.h"
#include "Game.h"
#include "DataBase.h"
#include "Validator.h"

using namespace std;

TriviaServer::TriviaServer()
{
	DataBase* db = new DataBase();
	_db = db;
	try
	{
		_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (_socket == INVALID_SOCKET)
		{
			throw std::exception(__FUNCTION__ " - socket");
		}
	}
	catch (exception e)
	{
		cout << e.what();
	}
	delete db;
}
TriviaServer:: ~TriviaServer()
{
	_roomsList.clear();
	_connectedUsers.clear();
	::closesocket(_socket);
}
void TriviaServer::serve()
{
	bindAndListen();
	thread handleMsg (&TriviaServer::handleRecievedMessages,this);
	handleMsg.detach();
	while (true)
	{
		// the handleclient thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}
void TriviaServer::bindAndListen()
{
	int port=1337;
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;
}
void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	cout << "Client accepted. Server and client can comunicate" << endl;
	// the function that handle the conversation with the client
	std::thread clienthandle(&TriviaServer::clientHandler, this, client_socket);
	clienthandle.detach();
}
void TriviaServer::clientHandler(SOCKET soc)
{
	int code = 0;
	try
	{
		code = Helper::getMessageTypeCode(soc);
		cout << code << endl;
		if (code != 0 || code != 121)
		{
			if (buildRecieveMessage(soc, code) == 0)
			{
				throw EXCEPTION_CONTINUE_EXECUTION;
				cout << "FAIL" << endl;
			}
			else
			{
				addRecievedMessage(buildRecieveMessage(soc,code));
			}
		}
		else
		{
			addRecievedMessage(buildRecieveMessage(soc, 121));
		}
	}
	catch (exception e)
	{
		addRecievedMessage(buildRecieveMessage(soc, 121));
	}
}
void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		User* user = msg->getUser();
		SOCKET soc = msg->getSock();
		handleSingout(msg);
		closesocket(soc);
	}
	catch (exception e)
	{
		cout << __FUNCTION__ << e.what() << endl;
	}
}
void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	unique_lock<std::mutex> locker(_mtxRecievedMessages);
	_queRcvMessages.push(msg);
	locker.unlock();
	_cond.notify_one();
}
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET soc, int code)
{
	RecievedMessage* msg = new RecievedMessage(soc,code);
	return msg;
}
RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET soc, int code, vector<string> values)
{
	RecievedMessage* msg = new RecievedMessage(soc, code, values);
	return msg;
}
void TriviaServer::handleRecievedMessages()
{
	unique_lock<std::mutex> locker(_mtxRecievedMessages);
	int code = 0;
	_cond.wait(locker, [&](){return !(this->_queRcvMessages.empty()); });
	RecievedMessage* msg = NULL;
	locker.unlock();
	msg = (_queRcvMessages.pop());
	msg->setUser(getUserBySocket(msg->getSock()));
	cout << msg->getSock() << endl;
	code = Helper::getMessageTypeCode(msg->getSock());
	cout << code << endl;
	switch (code)
	{
	case 0:
		safeDeleteUser(msg);
		break;
	case 121:
		safeDeleteUser(msg);
		break;
	case 200:
		handleSingin(msg);
		break;
	case 201:
		handleSingout(msg);
		break;
	case 203:
		cout << "ok" << endl;
		handleSingup(msg);
		cout << "ok" << endl;
		break;
	case 205:
		handleGetRooms(msg);
		break;
	case 207:
		handleGetUsersInRoom(msg);
		break;
	case 209:
		handleJoinRoom(msg);
		break;
	case 211:
		handleLeaveRoom(msg);
		break;
	case 213:
		handleCreateRoom(msg);
		break;
	case 215:
		handleCloseRoom(msg);
		break;
	case 217:
		handleStartGame(msg);
		break;
	case 219:
		handlePlayerAnswer(msg);
		break;
	case 222:
		handleLeaveGame(msg);
		break;
	case 223:
		handleGetBestScores(msg);
		break;
	case 225:
		addRecievedMessage(buildRecieveMessage(msg->getSock(),255));
		handleGetPersonalStatus(msg);
		break;
	case 299:
		cout << "bye" << endl;
		break;
	default:
		safeDeleteUser(msg);
		break;
	}
}
User* TriviaServer::handleSingin(RecievedMessage* recievedMessage)
{
	cout << "SINGIN" << endl;
	vector<string> values = recievedMessage->getValues();
	if (_db->isUserAndPassMatch(values[0], values[1]))
	{
		User* user = new User(values[0], recievedMessage->getSock());
		_connectedUsers.insert(pair<SOCKET, User*>(user->getSocket(),user));
		return user;
	}
	else
	{
		return NULL;
	}
}
bool TriviaServer::handleSingup(RecievedMessage* recievedMessage)
{
	cout << "SING-UP" << endl;
	int worked = 0;
	vector<string> values = recievedMessage->getValues();
	if (Validator::isPasswordValid(values[1]))
	{
		if (Validator::isUsernameValid(values[0]))
		{
			if (_db->isUserExists(values[0]))
			{
				addRecievedMessage(buildRecieveMessage(_socket, 121));
			}
			else
			{
				if (_db->addNewUser(values[0], values[1], values[2]))
				{
					worked = 1;
				}
				else
				{
					addRecievedMessage(buildRecieveMessage(_socket, 121));
				}
			}
		}
		else
		{
			addRecievedMessage(buildRecieveMessage(_socket, 121));
		}
	}
	else
	{
		addRecievedMessage(buildRecieveMessage(_socket, 121));
	}
	return worked;
}

void TriviaServer::handleSingout(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user)
	{
		_connectedUsers.erase(user->getSocket());
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
	}
	delete user;
}


void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	User* user = msg->getUser();
	if (user->leaveGame())
	{
		delete(user->getRoom());
	}
	delete user;
}
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	User* user = msg->getUser();
	Room* room = user->getRoom();
	Game* game = NULL;
	int id = room->getId();
	vector<User*> players = room->getUsers();
	try
	{
		game = new Game(players, id, *_db);
		_roomsList.erase(room->getId());
		game->sendFirstQuestion();
	}
	catch (exception e)
	{
		addRecievedMessage(buildRecieveMessage(_socket, 121));
	}
	delete user;
	delete room;
	delete game;
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	int num = 0;
	User* user = msg->getUser();
	Game* game = user->getGame();
	if (game != NULL)
	{
		if (!(game->handleAnswerFromUser(msg->getUser(), num, num)))//FIGURE WHAT INTEGERS!!!!!
		{
			delete(game);
		}
	}
	delete user;
	delete game;
}

User* TriviaServer::getUserByName(string name)
{
	int i = 0;
	User* useR = NULL;
	std::map<SOCKET, User*>::iterator it;
	vector<User*> users;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		users[i] = it->second;
		i++;
	}
	std::vector<User*>::iterator it2;
	for (it2 = users.begin(); it2 != users.end(); it2++)
	{
		User** user = &(*it2);
		if ((*user)->getUsername() == name)
		{
			useR = *user;
		}
	}
	delete useR;
	return useR;
}
User* TriviaServer::getUserBySocket(SOCKET soc)
{
	int i = 0;
	User* useR = NULL;
	std::map<SOCKET, User*>::iterator it;
	vector<User*> users;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		users[i] = it->second;
		i++;
	}
	std::vector<User*>::iterator it2;
	for (it2 = users.begin(); it2 != users.end(); it2++)
	{
		User** user = &(*it2);
		if ((*user)->getSocket() == soc)
		{
			useR = *user;
		}
	}
	delete useR;
	return useR;
}
Room* TriviaServer::getRoomById(int id)
{
	std::map<int,Room*>::iterator it;
	it = _roomsList.find(id);
	if (it != _roomsList.end())
	{
		return it->second;
	}
	else
	{
		return NULL;
	}
}


bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	int work = 0;
	int id = 1;
	User* user = msg->getUser();
	Room* room = NULL;
	vector<string> values = msg->getValues();
	if (user)
	{
		_roomIdSequence++;
		if (user->createRoom(NULL, values[0], NULL, NULL, NULL))
		{
			room = user->getRoom();
			id = room->getId();
			_roomsList.insert(pair<int, Room*>(id, room));
			work = 1;
		}
	}
	delete user;
	delete room;
	return work;
}
bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	int work = 1;
	User* user = msg->getUser();
	Room* room = user->getRoom();
	if (room)
	{
		if ((user->closeRoom()) != -1)
		{
			_roomsList.erase(room->getId());
		}
	}
	else
	{
		work = 0;
	}
	delete user;
	delete room;
	return work;
}
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	int work = 1;
	int id = 0;
	vector<string> values = msg->getValues();
	User* user = msg->getUser();
	Room* room = NULL;
	if (user)
	{
		id = stoi(values[0]);//DON'T KNOW IF THIS IS THE ROOM ID! NEED TO CHECK WICH PLACE IS THE ID!
		room = getRoomById(id);
		if (room)
		{
			user->joinRoom(room);
		}
		else
		{
			user->send("121");
		}
	}
	else
	{
		work = 0;
	}
	delete user;
	delete room;
	return work;
}
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	int work = 1;
	User* user = msg->getUser();
	Room* room = NULL;
	if (user)
	{
		room = user->getRoom();
		if (room)
		{
			user->leaveRoom();
		}
		else
		{
			work = 0;
		}
	}
	else
	{
		work = 0;
	}
	delete user;
	delete room;
	return work;
}
void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	int id;
	vector<string> values = msg->getValues();
	User* user = msg->getUser();
	Room* room = NULL;
	if (user)
	{
		id = stoi(values[0]);//DON'T KNOW IF THIS IS THE ROOM ID! NEED TO CHECK WICH PLACE IS THE ID!
		room = getRoomById(id);
		if (room)
		{
			user->send(room->getUsersListMessage());
		}
		else
		{
			user->send("121");
		}
	}
	delete user;
	delete room;
}
void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	User* user = msg->getUser();
	std::map<int,Room*>::iterator it;
	string msg2="205";
	string msg3;
	Room* room = NULL;
	int i = 0;
	for (it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		msg2 += "##";
		room = it->second;
		msg3 = room->getName();
		msg2.append(msg3);
		i++;
	}
	user->send(msg2);
	delete user;
	delete room;
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	User* user = msg->getUser();
	vector<string> values = _db->getBestScores();
	std::vector<string>::iterator it;
	string msg2 = "223";
	int i = 0;
	for (it = values.begin(); it != values.end(); it++)
	{
		msg2 += "##";
		msg2 += values[i];
		i++;
	}
	user->send(msg2);
	delete user;
}
void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	User* user = msg->getUser();
	vector<string> values = _db->getPersonalStatus(user->getUsername());
	std::vector<string>::iterator it;
	string msg2 = "225";
	int i = 0;
	for (it = values.begin(); it != values.end(); it++)
	{
		msg2 += "##";
		msg2 += values[i];
		i++;
	}
	user->send(msg2);
	delete user;
}