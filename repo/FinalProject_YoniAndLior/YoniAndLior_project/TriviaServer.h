#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <stdio.h>
#include <map>
#include <queue>
#include <mutex>
#include <thread>
#include <string>
#include <condition_variable>

class DataBase;
class Room;
class RecievedMessage;
class User;

using namespace std;

class TriviaServer
{
public:

	TriviaServer();
	~TriviaServer();

	void serve();

private:
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);

	User* handleSingin(RecievedMessage*);
	bool handleSingup(RecievedMessage*);
	void handleSingout(RecievedMessage*);
	
	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);

	
	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* buildRecieveMessage(SOCKET, int);
	RecievedMessage* buildRecieveMessage(SOCKET, int,vector<string>);
	
	//Geters:
	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);

	
	
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	User* _curUser;
	DataBase* _db;
	map<int, Room*> _roomsList;
	mutex _mtxRecievedMessages;
	condition_variable _cond;
	queue<RecievedMessage*> _queRcvMessages;
	int _roomIdSequence;
};