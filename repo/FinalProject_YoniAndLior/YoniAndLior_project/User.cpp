#include <string>
#include <iomanip>
#include <sstream>
#include "WSAInitializer.h"	
#include "TriviaServer.h"
#include "User.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "User.h"
#include"Question.h"
#include"Game.h"
#include "Validator.h"
#include "DataBase.h"
using std::string;
using std::vector;

User::User(string username, SOCKET sock) : _userName(username), _sock(sock)
{
	_currRoom = NULL;
	_currGame = NULL;
}

void User::send(string message)
{
	Helper::sendData(_sock, message);
}

string User::getUsername()
{
	return(_userName);
}

SOCKET User::getSocket()
{
	return(_sock);
}

Room* User::getRoom()
{
	return(_currRoom);
}

Game* User::getGame()
{
	return(_currGame);
}

void User::setGame(Game* gm)
{
	_currGame = gm;
	_currRoom = NULL;
}

void User::clearRoom()
{
	_currGame = NULL;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	bool state;
	if (_currRoom == NULL)
	{
		_currRoom = new Room(roomId, NULL, roomName, maxUsers, questionsNo, questionTime);
		Helper::sendData(getSocket(), "114");
		state = TRUE;
	}
	else
	{
		Helper::sendData(getSocket(), "114");
		state = FALSE;
	}
	return(state);
}

bool User::joinRoom(Room* newRoom)
{
	bool state = TRUE;
	bool parameter;
	if (_currRoom == NULL)
	{
		parameter = newRoom->joinRoom(this);
	}
	else
	{
		state = FALSE;
	}
	return(state);
}

void User::leaveRoom()
{
	if (_currRoom)
	{
		_currRoom->leaveRoom(this);
		_currRoom = NULL;
	}
}

int User::closeRoom()
{
	int state;
	if (_currRoom == NULL)
	{
		state = -1;
	}
	else
	{
		int ret = _currRoom->closeRoom(this);
		if (ret)
		{
			_currRoom = NULL;
		}
		state = 1;
	}
	return(state);
}

bool User::leaveGame()
{
	bool state;
	if (_currRoom)
	{
		_currRoom->leaveRoom(this);
		_currGame = NULL;
		state = TRUE;
	}
	else
	{
		state = FALSE;
	}

	return(state);
}