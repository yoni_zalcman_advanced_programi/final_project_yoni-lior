#pragma once
#include <WinSock2.h>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>

class Room;
class Helper;
class Game;

using namespace std;

class User
{
public:
	User(string, SOCKET);
	void send(string);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	void clearRoom();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:
	string _userName;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;

};
