#pragma once
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Validator
{
public:
	Validator();
	~Validator();
	static bool isPasswordValid(string str);
	static bool isUsernameValid(string str);
};